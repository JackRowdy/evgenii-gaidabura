#!/bin/bash

# Задание
# Написать скрипт: будильник (в нужный момент проиграть музыку, скрипт запрашивает время срабатывания
# и файл который надо проиграть). Скрипт должен иметь функции:
# создания, редактирования и удаления заданий будильника. Сторонние задания cron, скрипт
# должен игнорировать(не показывать).

CONST=/home/jackrowdy/EltexStudy/evgenii-gaidabura/Bash/lab2/sounds/sound
TAB=$(echo -e "\t")

function cCleaner {
	if [ -d temp ]; then
	rm -rf temp
	fi
	mkdir temp
	cd ./temp
}

function showMenu {
	clear
	case "$1" in
		0)
			echo Меню
			echo 1 - Добавить новый будильник
  			echo Список всех будильников:
  			echo -e "День недели \t Время \t\t Название мелодии \t Вкл./Выкл."
			count=1
			for ((i=0; i<${#array[*]}; i++))
			{
				let 'count+=1'
    			printf -- '%s' "$count - ${array[$i]}"
			}
			echo 0 - Выход
		;;
		1)
			echo Введите необходимые настройки через пробел:
			echo -e "1)Определите номер дня недели от 0 до 6 (от Вс до Сб соответственно)"
			echo -e "2)Далее задайте значение времени (часы, минуты через пробел)"
			echo -e "3)Потом выберите мелодию будильника (0-3)"
   			echo -e "4)А так же укажите его состояние: Вкл./Выкл., задав значение 1 или 0"
			echo -e "Пример: 1 2 1 3 0 (Соответствует Пн 02:01 мелодия3 Выкл.)"
		;;
		*)
			echo 1 - Вкл./Выкл.
  		    echo 2 - Редактировать
  		    echo 3 - Удалить
  		    echo 4 - Вернуться в главное меню
   		    echo 0 - Выход
		;;
	esac
}

function addClock {
	showMenu $num
	options 1
	addCronTab
	cd ..
	main
}

function choiceAlarmClock {
	showMenu $num
	read cin
    case "$cin" in
	1)
		trigger
		cd ..
		main
	;;
	2)
		editAlarmClock
	;;
	3)
		unset arr
		unset array
		sed -i "$(($num-1)) d" alarmListCrontab.tmp
		sed -i "$(($num-1)) d" alarmList.tmp
		readarray arr < alarmListCrontab.tmp
		readarray array < alarmList.tmp
		cat tempCron.tmp alarmListCrontab.tmp>cronTab.tmp
		crontab cronTab.tmp
		cd ..
		main
	;;
	4)
		main
	;;
	0)
		exit
	;;
	esac
}

function trigger {
	if [ "${array[$(($num-2))]:(-5):4}" = "Вкл." ]; then
		sed -i "$(($num-1)) s/Вкл/Выкл/" alarmListCrontab.tmp
		sed -i "$(($num-1)) s/^/#/" alarmListCrontab.tmp
		addCronTab
	else
		sed -i "$(($num-1)) s/Выкл/Вкл/" alarmListCrontab.tmp
		sed -i "$(($num-1)) s/^.//" alarmListCrontab.tmp
		addCronTab
	fi
}

function addCronTab {
	cat tempCron.tmp alarmListCrontab.tmp>cronTab.tmp
	crontab cronTab.tmp
}

function options {
	read day hours minutes number toggle
	arrDays=("Вс" "Пн" "Вт" "Ср" "Чт" "Пт" "Сб")
	DAY=("${arrDays[$day]}")
	if (($hours<10)); then HOURS=0$hours; else HOURS=$hours; fi
	if (($minutes<10)); then MINUTES=0$minutes; else MINUTES=$minutes; fi
 	if (($toggle)); then
 		toggle=Вкл.
 		comment=""
 	else 
 		toggle=Выкл.
 		comment=#
 	fi
 	if [ "$1" = "1" ]; then
 		arr=("${arr[@]}" "$comment$minutes $hours * * $day DISPLAY=:0 vlc "$CONST$number.mp3" &>/dev/null #alarm $DAY $TAB$TAB $HOURS:$MINUTES $TAB$TAB sound$number.mp3 $TAB$TAB $toggle")
    	printf -- '%s\n' "${arr[@]:(-1)}">>alarmListCrontab.tmp
    else
    	var="$comment$minutes $hours * * $day DISPLAY=:0 vlc \"$CONST$number.mp3\" &>/dev/null #alarm $DAY $TAB$TAB $HOURS:$MINUTES $TAB$TAB sound$number.mp3 $TAB$TAB $toggle"
	fi
}

function editAlarmClock {
	showMenu 1
	options 2
	sed -i "$(($num-1))c $var" alarmListCrontab.tmp
	addCronTab
    cd ..
    main
}

function main {
	cCleaner
	crontab -l|grep -v 'alarm.*'>>tempCron.tmp
	crontab -l|grep 'alarm.*'>alarmListCrontab.tmp
	readarray arr < alarmListCrontab.tmp
	crontab -l|grep -o 'alarm.*'| sed 's/alarm //'>alarmList.tmp
	readarray array < alarmList.tmp

	num=0
	showMenu $num
	read num
	if [ "$num" = 1 ]; then addClock
	elif [ "$num" = 0 ]; then
		cd ..
		if [ -d temp ]; then
	    	rm -rf temp
	    fi
		exit
	elif (($num + 0)) && [ "$num" != "" ]; then choiceAlarmClock
		else
		cd ..
		main
	fi
}

main