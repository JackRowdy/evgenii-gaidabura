#include <stdio.h>

float divide(int n, int *array)
{
	float div = array[0];
	for (int i = 1; i < n-1; i++)
		div /= array[i];
    return div;
}