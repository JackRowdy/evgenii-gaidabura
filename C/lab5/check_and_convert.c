#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int* check_and_convert(int n, char **digit)
{

	int *result;
	result = (int *) calloc(n, sizeof(int));
	if(!result)
	{
    	fprintf (stderr, "Ошибка при распределении памяти\n");
    	exit(1);
  	}

	 for (int i = 1; i < n; i++)
	 {
	 	for (int j = 0; j < strlen(digit[i]); j++)
	 	{
 			if ( !( isdigit(digit[i][j]) ) )
  			{
     			fprintf (stderr, "Неверно заданы параметры файла, используйте целые числа\n");
	 			exit(1);
  			}
  		}
   		result[i-1] = atoi(digit[i]);
	 }
    return result;
}