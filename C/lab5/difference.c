#include <stdio.h>

int difference(int n, int *array)
{
	int dif = array[0];
	for (int i = 1; i < n; i++)
		dif -= array[i];
    return dif;
}