#include <stdio.h>

int multiplication(int n, int *array)
{
	int mul = 1;
	for (int i = 0; i < n-1; i++)
		mul *= array[i];
    return mul;
}