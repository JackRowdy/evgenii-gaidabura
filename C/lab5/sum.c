#include <stdio.h>

int sum(int n, int *array)
{
	int s = 0;
	for (int i = 0; i < n; i++)
		s += array[i];
    return s;
}