/*ЗАДАНИЕ:
1.Разработать функции для выполнения арифметических операций по вариантам. 
2.Оформить статическую библиотеку функций и написать программу, ее использующую.
3.Переоформить библиотеку, как динамическую, но подгружать статически, при компиляции.
4.Изменить программу для динамической загрузки функций из библиотеки.

Варианты
1.Операции сложения и вычитания.
2.Операции умножения и деления.*/

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include "eltex.h"

#define RESET "\033[0m"
#define WHITE "\033[1;37m"

int main(int argc, char **argv)
{	
	void *library_handler = dlopen( "/home/jackrowdy/EltexStudy/evgenii-gaidabura/C/lab5/libeltexx.so", RTLD_LAZY );

	if (!library_handler){
    	fprintf(stderr,"dlopen() error: %s\n", dlerror());
    	exit(1);
	};

	int (*Fsum)(int, int*);
   	int (*Fdif)(int, int*);
   	float (*Fdiv)(int, int*);
   	int (*Fmul)(int, int*);

  	Fsum = dlsym(library_handler, "sum");
 	Fdif = dlsym(library_handler, "difference");
	Fdiv = dlsym(library_handler, "divide");
 	Fmul = dlsym(library_handler, "multiplication");

	int *array = NULL;
	
	if(argc < 3)
	{
		fprintf (stderr, "Мало аргументов. Используйте как минимум 2 числа <имя файла> <целлое_число> <целое_число>\n");
		exit(1);
	}
	else array = check_and_convert(argc, argv);

	printf("Сумма чисел равна %s %d %s \n", WHITE, (*Fsum)(argc, array), RESET);
	printf("Разность чисел равна %s %d %s \n", WHITE, (*Fdif)(argc, array), RESET);
	printf("Частное при последовательном делении чисел равно %s %.2f %s \n", WHITE, (*Fdiv)(argc, array), RESET);
	printf("Произведение чисел равно %s %d %s \n", WHITE, (*Fmul)(argc, array), RESET);
	printf("Произведение чисел равно %s %d %s \n", WHITE, array[0], RESET);

	// printf("Сумма чисел равна %s %d %s \n", WHITE, sum(argc, array), RESET);
	// printf("Разность чисел равна %s %d %s \n", WHITE, difference(argc, array), RESET);
	// printf("Частное при последовательном делении чисел равно %s %.2f %s \n", WHITE, divide(argc, array), RESET);
	// printf("Произведение чисел равно %s %d %s \n", WHITE, multiplication(argc, array), RESET);
	// printf("Произведение чисел равно %s %d %s \n", WHITE, array[0], RESET);

	dlclose(library_handler);
    return 0;
}
