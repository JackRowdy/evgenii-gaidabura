//Вариант 6
/*ЗАДАНИЕ:
Написать программу сортировки массива строк по вариантам.
Строки вводить с клавиатуры. Сначала пользователь вводит кол-во строк потом сами строки. 
Для тестирования использовать перенаправление ввода-вывода ($./myprog < test.txt). 
Память для строк выделять динамически с помощью функций malloc или calloc.
Ввод данных, сортировку и вывод результатов оформить в виде функций.
Сортировку делать с помощью qsort если возможно, если нет то писать свой вариант сортировки.
Входные и выходные параметры функции сортировки указаны в варианте. 
Входные и выходные параметры функций для ввода-вывода: 

Прототип функции для ввода одной строки 
length = inp_str(char* string, int maxlen); 
// length – длина строки 
// string – введенная строка 
// maxlen – максимально возможная длина строки (размерность массива string) 

Прототип функции для вывода одной строки.
void out_str(char* string, int length, int number);
// string – выводимая строка 
// length – длина строки 
// number – номер строки 

Расположить строки по убыванию количества слов 
Входные параметры:
1. Массив 
2. Размерность массива 
Выходные параметры:
1. Количество перестановок 
2. Максимальное количество слов  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_LEN 1024 /* максимальная длина строки */

int inp_str(char*, int);
int sorting(char**, int);
void out_str(char*, int, int);
void freeMas(char**, int);

int global = 0;

int inp_str(char* string, int maxlen){
	fgets(string, maxlen, stdin);
	int length = strlen(string);
    return length;
}

int sorting(char **mas, int maxstr){
	int tmp, len;
	int words = 0, count = 0;
	char *chTMP;
	int wordsMas[maxstr];//массив соответствия строка = количество слов
	for (int i = 0; i < maxstr; i++){
		len = strlen(mas[i]);
		words = 1;
		for(int j=0; j<len; j++)
			if(mas[i][j] == ' ') words++; //считаем количество пробелов(предполагая, что пользователь разделял слова одним пробелом)
		wordsMas[i] = words;
	}

	for(int i = 1 ; i < maxstr; i++)
		for(int j = 0 ; j < maxstr - i; j++)
			if (wordsMas[j] < wordsMas[j+1]) {           
				
				tmp = wordsMas[j];//сортируем временный массив(методом камешка)
				wordsMas[j] = wordsMas[j+1];
				wordsMas[j+1] = tmp;	
				
				chTMP = mas[j];//сортируем сам массив строк
				mas[j] = mas[j+1];
				mas[j+1] = chTMP;
				
				count++;//считаем количество перестановок
			}
	global = count;
	return wordsMas[0];
}

void out_str(char* string, int length, int number){
    number++;
    printf("%d  %d  %s", number, length, string);
}

void freeMas(char **mas, int count){
    for (int i = 0; i < count; i++)
        free(mas[i]); // освобождаем память для отдельной строки
    free(mas); // освобождаем памать для массива указателей на строки
}

int main(int argc, char **argv){
	char buffer[MAX_LEN];
	char **mas = NULL; //указатель на массив указателей на строки
	int N, len;
	scanf("%d\n", &N);
	mas = (char **)malloc(sizeof(char *)*N);// выделяем память для массива указателей
	for (int i = 0; i < N; i++){
		len = inp_str(buffer, MAX_LEN);
        mas[i] = (char *)malloc(sizeof(char)*len); //выделяем память для строки
		strcpy(mas[i], buffer); //копируем строку из буфера в массив указателей
    }
    printf("\nКоличество перестановок: %d\nМаксимальное количество слов: %d\n\n", global, sorting(mas, N));
    for (int i = 0; i < N; i++)
      	out_str(mas[i], strlen(mas[i]), i);
    
	freeMas(mas, N);
	return 0;
}
