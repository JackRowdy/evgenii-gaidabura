/*ЗАДАНИЕ:
2. 
Модифицировать программу, используя массив указателей на структуру и динамическое выделение памяти. 
Выполнить сортировку массива с помощью функции qsort. 
Способ сортировки массива приведен в варианте.
Для динамического выделения памяти используйте функцию malloc().
Для определения размера структуры в байтах удобно использовать операцию sizeof(), возвращающую целую константу: 
	struct ELEM *sp; 
	sp = malloc(sizeof(struct ELEM));
Вариант 10
Фамилия
Группа
Место прохождения практики
Оценка
Расположить записи в массиве в порядке убывания оценки */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_BASE 3 /* размер базы */

struct student{
    char surname[50];
    char group[50];
    char company[50];
    int grade;
};
typedef struct student students;

void readStudent(students *st){
    printf("Введите фамилию:\n");
    scanf("%s", st->surname);
    printf("Укажите группу:\n");
    scanf("%s", st->group);
    printf("Введите место прохождения практики:\n");
    scanf("%s", st->company);
    printf("Укажите оценку:\n");
    scanf("%d", &st->grade);
}

static int cmp(const void *p1, const void *p2){
    students * st1 = *(students**)p1;
    students * st2 = *(students**)p2;
    return st2->grade - st1->grade;
}

void printStudent(students *st){
    printf("Фамилия: %s\n", st->surname);
    printf("Группа: %s\n", st->group);
    printf("Предприятие/Завод: %s\n", st->company);
    printf("Оценка: %d\n", st->grade);
}

int main(int argc, char **argv){
    students **st = (students**)malloc(sizeof(students**)*MAX_BASE);
    for (int i = 0; i < MAX_BASE ; i++){
        st[i] = (students*) malloc(sizeof(students));
        readStudent(st[i]);
    }
	qsort(st, MAX_BASE, sizeof(students*), cmp);
    for (int i = 0; i < MAX_BASE; i++){
		printStudent(st[i]);
        free(st[i]);
    }
    free(st);
    return 0;
}
