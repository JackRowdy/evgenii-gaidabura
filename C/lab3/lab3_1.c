/*ЗАДАНИЕ:
1. 
Написать программу, работающую с базой данных в виде массива структур и выполняющую последовательный ввод данных в массив и последующую распечатку его содержимого. 
Состав структуры приведен в варианте.
Типы данных выбрать самостоятельно.
При написании программы следует использовать статические массивы структур или указателей на структуру.
Размерности массивов – 3–4. 
Ввод данных выполнить с помощью функций scanf().
Вариант 10
Фамилия 
Группа 
Место прохождения практики 
Оценка 
Расположить записи в массиве в порядке убывания оценки */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_BASE 3 /* размер базы */

struct student{
    char surname[50];
    char group[50];
    char company[50];
    int grade;
};
typedef struct student students;

int main(int argc, char **argv){
    students st[MAX_BASE];
    
    for (int i = 0; i < MAX_BASE ; i++){
		printf("Введите фамилию:\n");
		scanf("%s", st[i].surname);
		printf("Укажите группу:\n");
		scanf("%s", st[i].group);
		printf("Введите место прохождения практики:\n");
		scanf("%s", st[i].company);
		printf("Укажите оценку:\n");
		scanf("%d", &st[i].grade);
    }
    for (int i = 0; i < MAX_BASE ; i++){
		printf("Фамилия студента:%s\n", st[i].surname);
		printf("Группа:%s\n", st[i].group);
		printf("Предприятие/Завод:%s\n", st[i].company);
		printf("Оценка:%d\n", st[i].grade);
    } 
    return 0;
}
