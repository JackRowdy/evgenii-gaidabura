#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define FILE_NAME "Файл_"
#define FILE_EXTENSION ".txt"

void file_gen(int digit, int max_length)
{
	char* name = FILE_NAME;
    char* extension = FILE_EXTENSION;
    char* number = malloc(1);

    sprintf (number, "%d", digit);
    
    char* file_name_extension = malloc((strlen(name) + strlen(extension) + strlen(number)));
    sprintf(file_name_extension, "%s%d%s", name, digit, extension);
	FILE* fp = fopen(file_name_extension, "w");

    char mass;
    for(int i=0; i < max_length; ++i)
   	{
      	mass = (rand() % ('z'-'a'+2))+'a'-1;
       	if (mass < 'a') mass = ' ';
       	fwrite (&mass, 1, sizeof(mass), fp);
    }
    fclose (fp);
}