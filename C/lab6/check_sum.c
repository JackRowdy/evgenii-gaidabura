#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define FILE_NAME "Файл_"
#define FILE_EXTENSION ".txt"
#define RESET "\033[0m"
#define WHITE "\033[1;37m"

void check_sum(int digit)
{
	if(digit == -1) exit(0);
    pid_t pid = fork();

	char* name = FILE_NAME;
	char* extension = FILE_EXTENSION;
	char* number = malloc(1);

	sprintf (number, "%d", digit);
    
	char* file_name_extension = malloc((strlen(name) + strlen(extension) + strlen(number)));
	sprintf(file_name_extension, "%s%d%s", name, digit, extension);
	FILE* fp = fopen(file_name_extension, "r");
	
	char ch;
	int contol_sum = 0;
	int status;

	if (pid == -1)
	{
		perror("произошла ошибка");
		exit(0);
	}
	else if (pid == 0)
	{	
		check_sum(digit-1);
		exit(0);
	}
	else
		if (wait(&status) == -1)
			perror("wait() error");
		else if (WIFEXITED(status))
		{
			while((ch = fgetc(fp)) != EOF) contol_sum+=ch;
			printf("%s%s %s| Контрольная сумма равна%s %d\n", WHITE, file_name_extension, RESET, WHITE, contol_sum);
			fclose (fp);
			exit(0);
		}
}